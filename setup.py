#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "proxy",
    version = "1.0.0",
    url = 'https://github.com/ondrejsika/proxy',
    maintainer = 'Ondrej Sika',
    maintainer_email = 'ondrej@ondrejsika.com',
    description = 'Simple Twisted proxy',
    scripts = ['proxy'],
    install_requires = ['twisted'],
)
